const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class UserService {
    constructor(datafile) {
        this.datafile = datafile;
    }


    async getList() {
        const data = await this.getData();
        return data;
    }  

    async addEntry(email, password, firstName , lastName ) {
        const data = (await this.getData()) || [];
        
        const userID = data.length + 1; 
        data.unshift({ userID, email, password, firstName , lastName});
    
        return writeFile(this.datafile, JSON.stringify(data));
    }
    
    async getData() {
        const data = await readFile(this.datafile, 'utf8');
        if (!data) return [];
        return JSON.parse(data);
    }
}

module.exports = UserService;