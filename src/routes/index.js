const express = require("express");
const router = express.Router();
const doneRoute = require("./done");
const { check, validationResult } = require("express-validator");
const UserService = require("../UsersService");

const jwt = require('jsonwebtoken');

const validation = [
  check('task')
    .trim()
    .isLength({ min: 3 })
    .escape()
    .withMessage('A name is required')
] 

const validationForUser = [
  check('email')
  .trim()
  .isEmail()
  .normalizeEmail()
  .withMessage('A valid email address is required'),
]


module.exports = (params) => {
  const { tasksService, usersService } = params;
  const SECRET_KEY = 'your_very_secret_key_here';


  router.get("/", async (req, res) => {
    // Render the index page using the 'layout' template
    const error = req.session ? req.session.errors : false;
    const successMessage = req.session ? req.session.message : false;
    console.log(error);
    req.session = {};
    const tasks = await tasksService.getList();

    res.render("layout", {
      pageTitle: "Home",
      template: "index",
      tasks,
      error,
      successMessage,
    });
  });

  router.post("/", validation, async (req, res, next)=>{
    try{
      const errors = validationResult(req);
      console.log(req.body);
      const taskName = req.body.task;

      if (!errors.isEmpty()) {
        req.session = {
          errors: errors.array(),
        };
        return res.redirect('/');
      }
      await tasksService.addEntry(taskName);
      req.session = {
        message: 'Thank you for your feedback!',
      };
      return res.redirect('/');
    }catch(error){
      next(error);
    }
  });


  router.post('/api/tasks/:taskName/done', async (req, res) => {
    try {
      const updatedTask = await tasksService.markDone(req.params.taskName);
      res.json(updatedTask); // Send the updated task object back to the client
    } catch (error) {
      console.error('Error marking task as done:', error);
      res.status(404).send(error.message); // Send a 404 Not Found error message
    }
  });

  router.post('/api/register', validationForUser ,async (req, res) => {
    try {

      const error = req.session ? req.session.errors : false;
      const successMessage = req.session ? req.session.message : false;
      req.session = {};

      await usersService.addEntry(req.body.email, req.body.password, req.body.firstName, req.body.lastName);

      const users = await usersService.getList();

      res.json(users); // Send the updated task object back to the client
    } catch (error) {
      console.error('Error marking task as done:', error);
      res.status(404).send(error.message); // Send a 404 Not Found error message
    }
  });

  router.post('/api/login', async (req, res)=>{
    const { email, password } = req.body;
    const users = await usersService.getList();
    const user = users.find(user => user.email === email);

    if(user.password !== password){
      res.status(401).send("Loh");
    }

    const token = jwt.sign({ id: user.userID }, SECRET_KEY, { expiresIn: '5m' });
    req.session.userId = user.userID;
    res.status(200).send({ token});
  });

  const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    console.log(authHeader);
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) return res.sendStatus(401);
    jwt.verify(token, SECRET_KEY, (err, decoded) => {
      if (err) return res.sendStatus(403);
      req.userId = decoded.userId;
      console.log(req.userId);
      next();
    });
  };
  
  router.post('/api/task', authenticateToken, async (req, res) => {
    // Assuming userID is stored in the JWT token
    try {
      const { title, description} = req.body;
      const userID = req.session.userId; 
      console.log(userID)
      const tasks = await tasksService.getList(userID);
      const checkTask = tasks.find(tasks => tasks.taskName === title);

      if(checkTask){
        res.status(300).send("the same task");
      }else{
        await tasksService.addEntry(userID, title, description);
        const finalTasks = await tasksService.getList(userID);
        res.status(200).send(finalTasks);
      }
    } catch (error) {
      res.status(500).send({ error: 'Failed to fetch tasks' });
    }
  });

  router.delete('/api/delete/:taskName', authenticateToken, async (req, res) => {
    // Assuming userID is stored in the JWT token
    try {
      const taskName = req.params.taskName;
      console.log(taskName)
      const userID = req.session.userId; 
      console.log(userID)
      const response = await tasksService.deleteTask(userID, taskName);
      res.status(200).send(response)
    } catch (error) {
      res.status(400).send({ error: 'Failed to delete' });
    }
  });

  router.patch("/api/done/:taskName", authenticateToken, async(req, res) => {
      try {
        const taskName = req.params.taskName;
        const userID = req.session.userId; 
        console.log(userID)
        const response = await tasksService.markDone(userID, taskName);
        res.status(200).send(response)
      } catch (error) {
        res.status(400).send({ error: 'Failed to delete' });
      }
    }
  );

  router.patch("/api/:taskName/change", authenticateToken, async(req, res) => {
    try {
      const taskName = req.params.taskName;
      const taskDescription = req.body.taskDescription;
      const userID = req.session.userId; 
      console.log(userID)
      const response = await tasksService.changeTask(userID, taskName, taskDescription);
      res.status(200).send(response)
    } catch (error) {
      res.status(400).send({ error: 'Failed to delete' });
    }
  });

  router.get("/api/tasks", authenticateToken, async(req, res)=>{
    try{
      const userID = req.session.userId;
      const tasks = await tasksService.getList(userID);
      res.status(200).send(tasks)
    }catch(error){
      res.status(400).send({error:'Failed to download'})
    }
  });

  router.get("/api/doneTasks", authenticateToken, async(req, res)=>{
    try{
      const userID = req.session.userId;
      const tasks = await tasksService.getDoneList(userID);
      res.status(200).send(tasks)
    }catch(error){
      res.status(400).send({error:'Failed to download'})
    }
  })




  router.use("/done", doneRoute(params));

  return router;
};
