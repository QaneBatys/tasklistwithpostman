const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class TaskService {
    constructor(datafile) {
        this.datafile = datafile
    }


    async getList(userID) {
        const data = await this.getData();
        const tasks = data.filter(data => data.userID ===userID && data.done == false);
        return tasks;
    }

    async getDoneList(userID){
        const data = await this.getData();
        const tasks = data.filter(data => data.userID ===userID && data.done === true);
        return tasks;
    }

    async markDone( userID, taskName) {
        const data = await this.getData();
        const index = data.findIndex((task) =>task.taskName === taskName && task.userID === userID);

        if (index !== -1) {
          data[index].done = true;
          await writeFile(this.datafile, JSON.stringify(data));
          return data[index]; // Return the updated task for client-side updates
        } else {
          throw new Error(`Task '${taskName}' not found.`);
        }
  }
    

    async addEntry(userID, taskName, taskDescription) {
        const data = (await this.getData()) || [];
        data.unshift({ userID, taskName, taskDescription, done:false });
        return writeFile(this.datafile, JSON.stringify(data));
    }
    
    async deleteTask(userID, taskName){
        const data = await this.getData();
        const index = data.findIndex((task) =>task.taskName === taskName && task.userID === userID);
        console.log(index);


        const deleted = data.splice(index, 1);
        await writeFile(this.datafile, JSON.stringify(data));
        return deleted;
    }

    async changeTask(userID, taskName, taskDescription){    
        const data = await this.getData();
        const index = data.findIndex((task) =>task.taskName === taskName && task.userID === userID);

        if (index !== -1) {
          data[index].taskDescription = taskDescription;
          await writeFile(this.datafile, JSON.stringify(data));
          return data[index]; // Return the updated task for client-side updates
        } else {
          throw new Error(`Task '${taskName}' not found.`);
        }
    }

    async getData() {
        const data = await readFile(this.datafile, 'utf8');
        if (!data) return [];
        return JSON.parse(data);
    }
}

module.exports = TaskService;